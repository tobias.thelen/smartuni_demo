# SmartUni Demo

This is a demo repository with a Django applicaiton skeleton for the SmartUni study project.

## Installation

You need:
- Python >= 3.8
- git

### Manual installation via Shell
1. Create a directory for your code and the virtual enviroment and change to it:
    - `mkdir smartuni`
    - `cd smartuni`
    
2. Create virtual environemt
    - `python3 -m venv venv`
    
3. Activate virtual enviroment
    - Windows: `venv\Scripts\activate.bat`
    - Linux: `source venv/bin/activate`

4. Clone git repository and chage to the new directory
    - `git clone ...`
    - `cd smartuni_demo`

5. Install Python modules
   - `pip3 install -r requirements.txt`
    
6. Create database
    - `python3 manage.py migrate`
    
7. Create admin user
   - `python3 manage.py createsuperuser`
    
8. Start  server
   - `python3 manage.py runserver`
    
### Installation in PyCharm Professional IDE

1. Download and install PyCharm Professional from https://www.jetbrains.com/pycharm/download

2. If you don't have one already, get a free educational account from JetBrains: https://www.jetbrains.com/community/education/#students

3. Configure Python Interpreter

4. Create new project from GitHub

5. Install Python modules

6. Create database

7. Create admin user

8. Start server


## Files in the repository
- `smartuni_demo` -- This folder contains the entire Django environment
  - `readme.md`  -- This file
  - `manage.py`  -- The autmatically generated management script used to issue any django commands (e.g. `runserver`to start the development server)
  - `requirements.txt`  -- List of python modules needed by the application. This file is used to quickly install all necessary modules via `pip3 install -r requirements.txt`
  - `.gitignore` -- List of filename patterns to be excluded from version control
  - `db.sqlite3` -- The databse file containing the SQLite database (will be switched to real database system later)
  - `smartuni_demo` -- Django configuration for the entire project
     - `__init__.py` -- Empty file (for now). Tells Python to treat directory as a module.
     - `asgi.py` -- Automatically generated file for use with asynchronous webserver   
     - `settings.py`  -- The main settings file, all configuration (paths, DB credentials, django features used, ...) is done here
     - `urls.py`  -- The main URL dispatcher file. Controls what code will be executed depending on the request URL
     - `wsgi.py` -- Automatically generated file for use with synchronous webserver   
  - `planner`  -- The planner django app which will hold everything for our project at the beginning
     - `__init__.py` -- Empty file (for now). Tells Python to treat directory as a module
     - `admin.py`  -- register and configure own model classes for use with admin app  
     - `models.py` -- Definition of data structures (database objects) and data operations
     - `tests.py` -- Test cases for automatic testing  
     - `urls.py` -- The URL dispatcher file for our application.
     - `views.py` -- Code that connects user actions (requests), data and output
     - `migrations` -- Folder that contains automatically generated code for database changes. These files have to be part of the git repository!
     - `static` -- Folder that contains static files like javascript, CSS, images
         - `planner` -- All static files for the main application
            - `dashboard.css`  -- Central CSS file with styl definitions (uses bootstrap)
            - `dashboard.js` -- Main Javascript functions (no framework, just plan JS with jQuery)
     - `templates` -- Folder that contains the HTML files with placeholders and display logic (django templates)
         - `planner`  -- All application specific templates
             - `base.html` -- Base template for the entire application
             - `index.html` -- Start page
             - `about.html` -- About page 
         - `registration`  -- Templates for standard user authentication functions (login, registration, ...) 
             - `login.html`  -- Login form
    
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib import messages

# Create your views here.

@login_required
def index(request):
    page='index' # for highlighting current page
    return render(request, 'planner/index.html', locals())

def about(request):
    page='about' # for highlighting current page
    messages.success(request, 'You called the about page. Congratulations!')
    return render(request, 'planner/about.html', locals())
